﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Categories
    /// </summary>
    [WebService(Namespace = "http://www.couvax.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Categories : System.Web.Services.WebService
    {
        public string BreadCrumbOut;
        [WebMethod]
        public List<spListCategoriesResult> ListCategories(int MerchantId, string Password, Boolean ShowInactives)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCategories(MerchantId, ShowInactives).ToList();
        }
        [WebMethod]
        public List<spGetCategoryInfoResult> GetCategoryInfo(int MerchantId, string Password, int CategoryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetCategoryInfo(MerchantId, CategoryId).ToList();
        }
        [WebMethod]
        public List<spListCategoriesByProductArrayResult> ListCategoriesByProductArray(int MerchantId, string Password, string Array)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCategoriesByProductArray(MerchantId, Array).ToList();
        }
        [WebMethod]
        public int GetCategoryId(int MerchantId, string Password, string Tag)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetCategoryId(MerchantId, Tag).Single().Id;
        }
        [WebMethod]
        public List<spListChildCategoriesResult> ListChildCategory(int MerchantId, string Password, int ParentId, bool ShowInactives)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListChildCategories(MerchantId, ParentId,ShowInactives).ToList();
        }
        private void BuildBreadCrumb(int MerchantId, int CategoryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var a = SqlServer.spGetCategoryInfo(MerchantId, CategoryId).Single();
            if (a.ParentId > 0)
            {
                 BreadCrumbOut = BreadCrumbOut + a.ParentId.ToString() + ",";
                 BuildBreadCrumb(MerchantId, System.Convert.ToInt16(a.ParentId));
            }
        }
        [WebMethod]
        public List<spListCategoriesArrayResult> ListBreadCrumb(int MerchantId, string Password, int CategoryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            BuildBreadCrumb(MerchantId, CategoryId);
            string res = CategoryId.ToString() + "," + BreadCrumbOut;
            if (res.Substring(res.Length - 1, 1) == ",")
            {
                res = res.Substring(0,res.Length - 1);
            }
            return SqlServer.spListCategoriesArray(MerchantId, res, false).ToList();
        }
        [WebMethod]
        public List<spListCategoriesByProductResult> ListCategoriesByProduct(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCategoriesByProduct(MerchantId,ProductId).ToList();
        }
        [WebMethod]
        public int InsertNewCategory(int MerchantId, string Password, int ParentId,string Description,bool IsActive, int Order)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var t = SqlServer.spInsertNewCategory(MerchantId, ParentId, Description, IsActive, Order).Single();
            return (int)t.Id;
        }
    }
}
