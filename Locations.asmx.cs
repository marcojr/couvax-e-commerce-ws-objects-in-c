﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Locations
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Locations : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spListStatesResult> ListStates(int MerchantId, string Password,int CountryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListStates(CountryId).ToList();
        }
        [WebMethod]
        public List<spGetAddressResult> GetAddress(int MerchantId, string Password, string ZipCode, int CountryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetAddress(ZipCode,CountryId).ToList();
        }
        [WebMethod]
        public List<spListCitiesResult> ListCities(int MerchantId, string Password, int CountryId, string State)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCities(CountryId,State).ToList();
        }
    }
}
