﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Customers
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Customers : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spGetCustomerResult> GetCustomer(int MerchantId, string Password, int CustomerId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetCustomer(CustomerId, MerchantId).ToList();
        }
        [WebMethod]
        public List<spGetCustomerBySsnOrTaxIdResult> GetCustomerBySsnOrTaxId(int MerchantId, string Password, string SsnOrTaxId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetCustomerBySsnOrTaxId(MerchantId,SsnOrTaxId).ToList();
        }
        [WebMethod]
        public void IncreaseCustomerBalance(int MerchantId, string Password, int CustomerId, decimal Amount)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spIncreaseCustomerBalance(MerchantId, CustomerId, Amount);
        }
        [WebMethod]
        public List<spCheckEmailSSNResult> CheckEmailSSN(int MerchantId, string Password, string Email, string SSN)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spCheckEmailSSN(MerchantId,Email,SSN).ToList();
        }
        [WebMethod]
        public List<spListCustomersByDateResult> ListCustomersByDate(int MerchantId, string Password, DateTime Date)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCustomersByDate(MerchantId, Date).ToList();
        }
        [WebMethod]
        public List<spInsertNewCustomerResult> InsertNewCustomer(int MerchantId, string Password, string Email, string cPassword, string Name,DateTime? BirthDay, string SSN, string Address, string Number, string Complement, int CityId, string Zip, string FixedPhone, string ComplFixedPhone, string Mobile, string BusinessOrIndividual, string Neighborhood, string Gender, string FreeText1, string FreeText2, string FreeText3, string Status)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            string z = Zip;
            if (Zip.Trim().Length == 8)
            {
                z = Zip.Trim() + "0";
            }
            return SqlServer.spInsertNewCustomer(MerchantId, Email,
                                                            cPassword,
                                                            Name,
                                                            BirthDay,
                                                            SSN,
                                                            Address,
                                                            Number,
                                                            Complement,
                                                            CityId,
                                                            z,
                                                            FixedPhone,
                                                            ComplFixedPhone,
                                                            Mobile,
                                                            System.Convert.ToChar(BusinessOrIndividual),
                                                            Neighborhood,
                                                            System.Convert.ToChar(Gender),
                                                            FreeText1,
                                                            FreeText2,
                                                            FreeText3,System.Convert.ToChar(Status)).ToList();
        }
        [WebMethod]
        public string GenerateCustomerToken(int MerchantId, string Password, string Email)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var r = SqlServer.spGenerateToken(MerchantId, Email.Trim());
            string y = "";
            if (r == null)
            {
                y= "";
            }
            else
            {
                y= r.Single().Token.ToString();
            }
            return y;
        }
        [WebMethod]
        public void ChangeCustomerPassword(int MerchantId, string Password, string Token, string NewPassword)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            Guid guid = new Guid(Token);
            SqlServer.spChangeCustomerPassword(MerchantId, guid, NewPassword);
        }
        [WebMethod]
        public void UpdateCustomer(int MerchantId, string Password, int CustomerId, string Name, DateTime? BirthDay, string SSN, string Address, string Number, string Complement, int CityId, string Zip, string FixedPhone, string ComplFixedPhone, string Mobile, string Neighborhood, string Gender, string FreeText1, string FreeText2, string FreeText3)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spUpdateCustomer(CustomerId,
                                                            MerchantId,
                                                            Name,
                                                            BirthDay,
                                                            SSN,
                                                            Address,
                                                            Number,
                                                            Complement,
                                                            CityId,
                                                            Zip,
                                                            FixedPhone,
                                                            ComplFixedPhone,
                                                            Mobile,
                                                            Neighborhood,
                                                            System.Convert.ToChar(Gender),
                                                            FreeText1,
                                                            FreeText2,
                                                            FreeText3);
        }
        [WebMethod]
        public int ValidateCustomer(int MerchantId, string Password, string Email, string CustomerPassword)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spValidateCustomer(MerchantId,Email,CustomerPassword).Single().CustomerId;       
        }
        [WebMethod]
        public void UpdateCustomerEmail(int MerchantId, string Password, int CustomerId, string Email)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spUpdateCustomerEmail(MerchantId,CustomerId,Email);
        }
        
    }
}
