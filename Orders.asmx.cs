﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Orders
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Orders : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spCreateOrderResult> CreateOrder(int MerchantId, string Password, string Session, int CustomerId, string DeliveryAddress, string DeliveryNumber, string DeliveryComplement, string DeliveryNeighborhood, string DeliveryZip, int DeliveryCityId, int PaymentTypeId, int Installments, decimal FreightValue, string IpAddress, int Deadline)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spCreateOrder(
                    MerchantId,
                    Session,
                    CustomerId,
                    DeliveryAddress,
                    DeliveryNumber,
                    DeliveryComplement,
                    DeliveryNeighborhood,
                    DeliveryZip,
                    DeliveryCityId,
                    PaymentTypeId,
                    Installments,
                    FreightValue, IpAddress,Deadline).ToList();
        }
        [WebMethod]
        public List<spListPendingOrdersResult> ListPendingOrders(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListPendingOrders(MerchantId).ToList();
        }
        public void SetOrderTerms(int MerchantId, string Password, int OrderId, string Terms)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetOrderTerms(MerchantId,OrderId,Terms);
        }
        [WebMethod]
        public List<spListCaptureQueueResult> ListCaptureQueue(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCaptureQueue(MerchantId).ToList();
        }
        [WebMethod]
        public List<spListOrdersResult> ListOrders(int MerchantId, string Password, int CustomerId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListOrders(MerchantId, CustomerId).ToList();
        }
        [WebMethod]
        public List<spListOrderItemsResult> ListOrderItems(int MerchantId, string Password, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListOrderItems(MerchantId, OrderId).ToList();
        }
        [WebMethod]
        public List<spGetOrderResult> GetOrder(int MerchantId, string Password, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetOrder(MerchantId, OrderId).ToList();
        }
        [WebMethod]
        public List<spListAntifraudQueueResult> ListAntifraudQueue(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListAntifraudQueue(MerchantId).ToList();
        }
        [WebMethod]
        public List<spListOrdersFeedbackPendingResult> ListOrdersFeedbackPending(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListOrdersFeedbackPending(MerchantId).ToList();
        }
        [WebMethod]
        public void FlagOrderFeedback(int MerchantId, string Password, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spFlagOrderFeedback(MerchantId,OrderId);
        }
        [WebMethod]
        public List<spListOrdersByDateResult> ListOrdersByDate(int MerchantId, string Password,DateTime Date)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListOrdersByDate(MerchantId,Date).ToList();
        }
        [WebMethod]
        public List<spListUnpaidOrdersResult> ListUnpaidOrders(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListUnpaidOrders(MerchantId).ToList();
        }
        [WebMethod]
        public void FlagOrderAsPaid(int MerchantId, string Password, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spFlagOrderAsPaid(MerchantId,OrderId);
        }
        [WebMethod]
        public List<spListOrdersByMonthResult> ListOrdersByMonth(int MerchantId, string Password, int Month,int Year)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListOrdersByMonth(MerchantId,Month,Year).ToList();
        }
    }
}
