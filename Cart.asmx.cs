﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Cart
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Cart : System.Web.Services.WebService
    {
        [WebMethod]
        public decimal InsertItemOnCart(int MerchantId, string Password,string CookieId, int Sku, bool IsGift, int Quantity)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            decimal d=0;
            for (int i = 0; i < Quantity ; i++)
            {
                var e = SqlServer.spInsertItemOnCart(MerchantId, CookieId,Sku, IsGift).Single();
                d = System.Convert.ToDecimal(e.TotalOnCart);
            }
            return d;
        }
        [WebMethod]
        public decimal RemoveItemOnCart(int MerchantId, string Password, string CookieId, int Sku, int Quantity)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            decimal d = 0;
            for (int i = 0; i < Quantity; i++)
            {
                var g =SqlServer.spRemoveItemOnCart(MerchantId, Sku,CookieId).Single();
                d = System.Convert.ToDecimal(g.TotalOnCart);
            }
            return d;
        }
        [WebMethod]
        public decimal RemoveSkuOnCart(int MerchantId, string Password, string CookieId, int Sku)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            decimal d = 0;
            var g = SqlServer.spRemoveSkuOnCart(MerchantId, Sku, CookieId).Single();
            d = System.Convert.ToDecimal(g.TotalOnCart);
            return d;
        }
        [WebMethod]
        public List<spListCartResult> ListCart(int MerchantId, string Password, string CookieId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListCart(MerchantId,CookieId).ToList();
        }
        [WebMethod]
        public void ClearCart(int MerchantId, string Password, string CookieId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spClearCart(MerchantId, CookieId);
        }
    }
}
