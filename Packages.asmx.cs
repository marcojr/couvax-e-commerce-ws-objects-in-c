﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Pacotes
    /// </summary>
    [WebService(Namespace = "http://www.couvax.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Packages : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spListPackagesResult> ListPackages(int MerchantId, string Password, string Order)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            //if (Order.ToLower() != "description" || Order.ToLower() != "order") { throw new UnauthorizedAccessException("Invalid Sort Argument"); }
            return SqlServer.spListPackages(MerchantId, Order).ToList();
        }
    }
}
