﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Buscas
    /// </summary>
    [WebService(Namespace = "http://www.couvax.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Searches : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spListTagCloudResult> ListTagCloud(int MerchantId, string Password, int MaxLevels, int Sensitive)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListTagCloud(MerchantId, MaxLevels, Sensitive).ToList();
        }
        [WebMethod]
        public List<spSearchActiveProductsResult> SearchActiveProducts(int MerchantId, string Password, string TextCriteria, int CategoryId, string SortArray,decimal MinPrice, decimal MaxPrice)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spSearchActiveProducts(MerchantId,TextCriteria,CategoryId,SortArray,MinPrice,MaxPrice).ToList();
        }
        [WebMethod]
        public List<spSearchProductsLevel2Result> SearchProductsLevel2(int MerchantId, string Password, string Criteria, int PageNumber, int RecordsPerPage)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spSearchProductsLevel2(MerchantId, Criteria, PageNumber, RecordsPerPage).ToList();
        }
        [WebMethod]
        public List<spSearchProductsLevel2ByCategoryResult> SearchProductsLevel2ByCategory(int MerchantId, string Password, string Criteria, int CategoryId, int PageNumber, int RecordsPerPage)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spSearchProductsLevel2ByCategory(MerchantId, Criteria, PageNumber, RecordsPerPage,CategoryId).ToList();
        }
        [WebMethod]
        public List<spSearchProductsLevel3Result> SearchProductsLevel3(int MerchantId, string Password, string Criteria)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spSearchProductsLevel3(MerchantId, Criteria).ToList();
        }
    }
}
