﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Webshops
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Webshops : System.Web.Services.WebService
    {

        [WebMethod]
        public int CreateWebShop(string Signature, string Name, string FormalName, string Description, string Keywords, char BusinessOrIndividual, string SsnOrCorpTax, string Password, bool InitialStatus, string AdministrativePassword, string BaseUrl, string Owner, string Address, string Phone, string Email)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (Signature != "12qPiraYankee") { throw new UnauthorizedAccessException("Access is Denied"); }
            return System.Convert.ToInt16(SqlServer.spCreateWebShop(Signature,Name,FormalName,Description,Keywords,BusinessOrIndividual,SsnOrCorpTax,Password,InitialStatus,AdministrativePassword,BaseUrl,Owner,Address,Phone,Email).Single().MerchantId);
        }
        [WebMethod]
        public string GetParameter(int MerchantId, string Password, string Parameter)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetParameter(MerchantId, Parameter).Single().Value;
        }
        [WebMethod]
        public void SetParameter(int MerchantId, string Password, string Parameter, string Value)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetParameter(MerchantId, Parameter,Value);
        }
        [WebMethod]
        public List<spListAllParametersResult> ListAllParameters(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListAllParameters(MerchantId).ToList();
        }
    }
}
