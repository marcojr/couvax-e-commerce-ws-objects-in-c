﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for ProdProperties
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProdProperties : System.Web.Services.WebService
    {

        [WebMethod]
        public List<spListPropertiesResult> ListProperties(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProperties(MerchantId).ToList();
        }
        [WebMethod]
        public List<spListPropertyItemsResult> ListPropertyItems(int MerchantId, string Password, int PropertyId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListPropertyItems(MerchantId, PropertyId).ToList();
        }
        [WebMethod]
        public int InsertNewProperty(int MerchantId, string Password, string Name)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var r = SqlServer.spInsertNewProperty(MerchantId, Name).Single();
            return (int)r.Id;
        }
        [WebMethod]
        public List<spInsertNewPropertyItemResult> InsertNewPropertyItem(int MerchantId, string Password, int PropertyId, string Name)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spInsertNewPropertyItem(MerchantId, PropertyId, Name).ToList();
        }
    }
}
