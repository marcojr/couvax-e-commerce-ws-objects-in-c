﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Couvax.BrazilFcontrolPlaceTransaction;

namespace Couvax
{
    /// <summary>
    /// Summary description for Antifraud
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Antifraud : System.Web.Services.WebService
    {

        [WebMethod]
        public string SendToAnalysisQueue(int MerchantId, string Password, int AntiFraudProvider, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            int ret = 0;
            string retx = "";
            if (AntiFraudProvider == 1)
            {
                //F-Control
                var merchant = SqlServer.spGetMerchant(MerchantId).Single();
                var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
                var customer = SqlServer.spGetCustomer(order.CustomerId, MerchantId).Single();
                var zz = SqlServer.spListOrderItems(MerchantId, OrderId);
                var items = zz.ToArray();
                SqlServer.spSetAntiFraudeProviderOnOrder(MerchantId, OrderId, AntiFraudProvider);
                var control = new WSFControl2();
                var user = new WsUsuario();
                var w4 = new WsTransacao4();
                var customer_data = new WsComprador();
                var delivery = new WsEntrega3();
                var delivery_addr = new WsEndereco();
                var customer_addr = new WsEndereco();
                user.Login = merchant.AntiFraudLogin;
                user.Senha = merchant.AntriFraudPassword;
                w4.DadosUsuario = user;
                customer_data.NomeComprador = customer.Name;
                customer_data.Sexo = customer.Gender.ToString();
                customer_addr.Rua = customer.Address;
                customer_addr.Numero = customer.Number;
                customer_addr.Complemento = customer.Complement;
                customer_addr.Bairro = customer.Neighborhood;
                customer_addr.Cidade = customer.City;
                customer_addr.Estado = customer.State;
                customer_addr.Pais = customer.Country;
                customer_addr.Cep = customer.Zip.Replace("-", "");
                customer_data.CpfCnpj = customer.SSN;
                customer_data.DddTelefone = customer.FixedPhone.Substring(0, 2);
                customer_data.NumeroTelefone = customer.FixedPhone.Substring(2, 8);
                customer_data.DddCelular = customer.Mobile.Substring(0, 2);
                customer_data.NumeroCelular = customer.Mobile.Substring(2, 8);
                customer_data.Email = customer.Email;
                customer_data.IP = order.IpAddress;
                customer_data.Endereco = customer_addr;
                w4.DadosComprador = customer_data;
                delivery_addr.Rua = order.DeliveryAddress;
                delivery_addr.Numero = order.DeliveryNumber;
                delivery_addr.Complemento = order.DeliveryComplement;
                delivery_addr.Bairro = order.Neighborhood;
                delivery_addr.Cidade = order.City;
                delivery_addr.Estado = order.State;
                delivery_addr.Pais = customer.Country;
                delivery_addr.Cep = order.DeliveryCep.Replace("-", "");
                delivery.Endereco = delivery_addr;
                delivery.NomeEntrega = customer.Name;
                delivery.DddTelefone = customer_data.DddTelefone;
                delivery.NumeroTelefone = customer_data.NumeroTelefone;
                w4.DadosEntrega = delivery;
                w4.CodigoPedido = OrderId.ToString();
                w4.QuantidadeItensDistintos = items.Length;
                int tot_items = 0;
                WsProduto3 [] products = new WsProduto3[items.Length];
                for (int i = 0; i < items.Length; i++)
                {
                    var product = new WsProduto3();
                    tot_items = tot_items + items[i].Quantity;
                    product.Codigo = items[i].Id.ToString();
                    product.Descricao = items[i].Name;
                    product.ValorUnitario = System.Convert.ToInt32((order.FreightValue * 100));
                    product.Quantidade = items[i].Quantity;
                    var cat = SqlServer.spListCategoriesByProduct(MerchantId, items[i].Id).ToArray();
                    if (cat.Length > 0)
                    {
                        product.Categoria = cat[0].Name;
                    }
                    products[i] = product;
                }
                w4.Produtos = products;
                w4.QuantidadeTotalItens = tot_items;
                w4.ValorTotalCompra = System.Convert.ToInt32((order.FreightValue * 100 + order.ProductsSum * 100));
                w4.ValorTotalFrete = System.Convert.ToInt32((order.FreightValue * 100));
                w4.DataCompra = order.Date;
                w4.Pagamentos = new WsPagamento2[1];
                w4.Pagamentos[0] = new WsPagamento2();
                if (order.PaymentType == "Visa")
                {
                    w4.Pagamentos[0].MetodoPagamento = (MetodoPagamento)2;
                }
                if (order.PaymentType == "Mastercard")
                {
                    w4.Pagamentos[0].MetodoPagamento = (MetodoPagamento)3;
                }
                w4.Pagamentos[0].NumeroParcelas = order.Installments;
                w4.Pagamentos[0].Valor = w4.ValorTotalCompra;
                var resultado = control.enfileirarTransacao4(w4);
                string[] msg = resultado.Mensagem.Split('|'); ;
                ret = System.Convert.ToInt32(msg[0]);
                SqlServer.spUpdateOrderAntiFraudeStatus(MerchantId, OrderId, ret);
                retx = resultado.Mensagem;
            }
            return retx;
        }
        [WebMethod]
        public void CaptureAnalisys(int MerchantId, string Password, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var merchant = SqlServer.spGetMerchant(MerchantId).Single();
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            //F-Control
            var control = new WSFControl2();
            WsAnalise2 result = control.capturarResultadoEspecifico2(merchant.AntiFraudLogin, merchant.AntriFraudPassword,OrderId.ToString());
            SqlServer.spUpdateOrderAntiFraudeStatus(MerchantId, OrderId,result.Status);
            if (result.Status == 2 || result.Status == 7)
            {
                SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AAC");
            }
            if (result.Status == 10)
            {
                SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "FCO");
            }
            if (result.Status == 6)
            {
                SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "CFS");
            }
            if (result.Status == 3)
            {
                SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "CAN");
            }
        }
    }
}
