﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Sorts
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Sorts : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spListSortItemsResult> ListSortItems(int MerchantId, string Password, int SortId, string Order)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            //if (Order.ToLower() != "description" || Order.ToLower() != "order") { throw new UnauthorizedAccessException("Invalid Sort Argument"); }
            return SqlServer.spListSortItems(MerchantId, SortId, Order.ToLower()).ToList();
        }
        [WebMethod]
        public List<spListSortItemsInCategoryResult> ListSortItemsInCategory(int MerchantId, string Password, int SortId, int CategoryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            //if (Order.ToLower() != "description" || Order.ToLower() != "order") { throw new UnauthorizedAccessException("Invalid Sort Argument"); }
            return SqlServer.spListSortItemsInCategory(MerchantId, SortId, CategoryId).ToList();
        }
        [WebMethod]
        public List<spListSortsResult> ListSorts(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            //if (Order.ToLower() != "description" || Order.ToLower() != "order") { throw new UnauthorizedAccessException("Invalid Sort Argument"); }
            return SqlServer.spListSorts(MerchantId).ToList();
        }
        [WebMethod]
        public List<spGetSortItemInfoResult> GetSortItemInfo(int MerchantId, string Password,int SortId, int SortCriteriaId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetSortItemInfo(MerchantId, SortId,SortCriteriaId).ToList();
        }
        [WebMethod]
        public int GetSortId(int MerchantId, string Password, string Description)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetSortId(MerchantId, Description).Single().Id;
        }
        [WebMethod]
        public List<spListProductSortsOnArrayResult> ListProductSortsOnArray(int MerchantId, string Password, string ProductArray)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductSortsOnArray(MerchantId, ProductArray).ToList();
        }
        [WebMethod]
        public int GetSortItemId(int MerchantId, string Password, string TagOrName)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetSortItemId(MerchantId, TagOrName).Single().Id;
        }
        [WebMethod]
        public List<spInsertNewSortResult> InsertNewSort(int MerchantId, string Password, string Name)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spInsertNewSort(MerchantId, Name).ToList();
        }
        [WebMethod]
        public List<spInsertNewSortItemResult> InsertNewSortItem(int MerchantId, string Password, int SortId, string Name, int Order)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spInsertNewSortItem(MerchantId, SortId, Name, Order).ToList();
        }
    }
}
