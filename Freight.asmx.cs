﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Couvax
{
    /// <summary>
    /// Summary description for Freight
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Freight : System.Web.Services.WebService
    {
        [WebMethod]
        public List<oc_CalculaFreteResult> CalcOfficeChairFreight(int MerchantId, string Password, string Zip, decimal Profit, int Quantity, decimal CorreiosPrice)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.oc_CalculaFrete(Zip, Profit, Quantity, CorreiosPrice).ToList();
        }
        [WebMethod]
        public string CalcDomesticFreightCart(int MerchantId, string Password, string CookieId, string Country, string Carrier, string CarrierUserName, string CarrierPassword, string Service, string ZipFrom, string ZipTo, string Parameters)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            string ret = "";
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var items = SqlServer.spListCart(MerchantId, CookieId).ToArray();
            int W = 0;
            int H = 0;
            int WD = 0;
            int D = 0;
            decimal TOT = 0;

            for (int i = 0; i < items.Length; i++)
            {
                var pid = SqlServer.spGetProduct(MerchantId, items[i].Id).Single();
                W = W + (System.Convert.ToInt16(pid.Weight) * System.Convert.ToInt16(items[i].Quantity));
                H = H + (System.Convert.ToInt16(pid.Height) * System.Convert.ToInt16(items[i].Quantity));
                if (System.Convert.ToInt16(pid.Width) > WD)
                {
                    WD = System.Convert.ToInt16(pid.Width);
                }
                if (System.Convert.ToInt16(pid.Deep) > D)
                {
                    D = System.Convert.ToInt16(pid.Deep);
                }
            }
            ret = "weight=$w,cube=$c,days=$d,price=$p,error=$e";
            string ret_w = W.ToString();
            string ret_d = "";
            string ret_p = "";
            string ret_e = "";
            //Cubagem: Soma das alturas + Max(Comprimento) + Max(Profundidade);
            if (Country.ToLower() == "pt-br")
            {
                if (Carrier.ToLower() == "correios")
                {
                    var correios = new BrazilCorreios.CalcPrecoPrazoWS();
                    var z = correios.CalcPrecoPrazo(CarrierUserName,
                            CarrierPassword,
                            Service,
                            ZipFrom,
                            ZipTo,
                            ((decimal)((decimal)W / (decimal)1000)).ToString().Replace(",", "."),
                            1,  //formato
                            D,  //comprimento
                            H,  //altura
                            WD,  //largura
                            0,
                            "N",
                            TOT,
                            "N").Servicos.Single();
                    ret_p = z.Valor.Replace(",", ".");
                    ret_d = z.PrazoEntrega.ToString();
                    ret_e = z.Erro;
                }
            }
            ret = ret.Replace("$w", ret_w);
            ret = ret.Replace("$d", ret_d);
            ret = ret.Replace("$p", ret_p);
            ret = ret.Replace("$c", WD.ToString() + "x" + D.ToString() + "x" + H.ToString());
            ret = ret.Replace("$e", ret_e);
            return ret;
        }
        [WebMethod]
        public string CalcDomesticProductPriceUnity(int MerchantId, string Password, int ProductId, string Country, string Carrier, string CarrierUserName, string CarrierPassword, string Service, string ZipFrom, string ZipTo, string Parameters)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            string ret = "";
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            decimal TOT = 0;
            var pid = SqlServer.spGetProduct(MerchantId, ProductId).Single();
            ret = "days=$d,price=$p,error=$e";
            string ret_p ="";
            string ret_d ="" ;
            string ret_e = "";
            if (Country.ToLower() == "pt-br")
            {
                if (Carrier.ToLower() == "correios")
                {
                    var correios = new BrazilCorreios.CalcPrecoPrazoWS();
                    var z = correios.CalcPrecoPrazo(CarrierUserName,
                            CarrierPassword,
                            Service,
                            ZipFrom,
                            ZipTo,
                            pid.Weight.ToString().Replace(",", "."),
                            1,  //formato
                            (decimal)pid.Deep,  //comprimento
                            (decimal)pid.Height,  //altura
                            (decimal)pid.Width,  //largura
                            0,
                            "N",
                            TOT,
                            "N").Servicos.Single();
                    ret_p = z.Valor.Replace(",", ".");
                    ret_d = z.PrazoEntrega.ToString();
                    ret_e = z.Erro;
                }
            }
            ret = ret.Replace("$d", ret_d);
            ret = ret.Replace("$p", ret_p);
            ret = ret.Replace("$e", ret_e);
            return ret;
        }
        [WebMethod]
        public string CalcDomesticFreight(int MerchantId, string Password, string Country, string Carrier, string CarrierUserName, string CarrierPassword, string Service, string ZipFrom, string ZipTo, string Parameters, int Weight, int Height, int Lenght, int Width)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            string ret = "";
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            decimal TOT = 0;
            ret = "days=$d,price=$p,error=$e";
            string ret_p = "";
            string ret_d = "";
            string ret_e = "";
            if (Country.ToLower() == "pt-br")
            {
                if (Carrier.ToLower() == "correios")
                {
                    var correios = new BrazilCorreios.CalcPrecoPrazoWS();
                    var z = correios.CalcPrecoPrazo(CarrierUserName,
                            CarrierPassword,
                            Service,
                            ZipFrom,
                            ZipTo,
                            Weight.ToString().Replace(",", "."),
                            1,  //formato
                            (decimal)Lenght,  //comprimento
                            (decimal)Height,  //altura
                            (decimal)Width,  //largura
                            0,
                            "N",
                            TOT,
                            "N").Servicos.Single();
                    ret_p = z.Valor.Replace(",", ".");
                    ret_d = z.PrazoEntrega.ToString();
                    ret_e = z.Erro;
                }
            }
            ret = ret.Replace("$d", ret_d);
            ret = ret.Replace("$p", ret_p);
            ret = ret.Replace("$e", ret_e);
            return ret;
        }
    
    }
}
