﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Text;
using BoletoNet;
using System.IO;

namespace Couvax
{
    /// <summary>
    /// Summary description for Pmgs
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Pmgs : System.Web.Services.WebService
    {
        [WebMethod]
        public string GetPaymentType(int MerchantId, string Password, int PaymentTypeId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetPaymentType(MerchantId, PaymentTypeId).Single().ToString();
        }
        [WebMethod]
        public string GenerateBoletoFromPsp(int MerchantId, string Password, int OrderId, int Rebate, string urlLogo, string Instructions, int DaysToDue, int Psp)
        {
            string ret = "";
             SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var customer = SqlServer.spGetCustomer(order.CustomerId,MerchantId).Single();
            var merchant = SqlServer.spGetMerchant(MerchantId).Single();
            string cpf_cnpj = merchant.SscOrFtid;
            cpf_cnpj = cpf_cnpj.Replace(".","");
            cpf_cnpj = cpf_cnpj.Replace("-", "");
            cpf_cnpj = cpf_cnpj.Replace("/", "");
            cpf_cnpj = cpf_cnpj.Replace("\\", "");
            var p = (from c in SqlServer.LOJA_PMGs
            where c.ID_LOJA.Equals(MerchantId)
            where c.ID_PMG.Equals(Psp)
            select c).Single();
            if (p.ID_PMG == 5)
            {
                string val = (System.Convert.ToDecimal(order.ProductsSum) + System.Convert.ToDecimal(order.FreightValue)).ToString("#0.00");
                string uri = "https://comercio.locaweb.com.br/comercio.comp";
                val = val.Replace(",", ".");
                uri = uri + "?identificacao=" + p.CONTA;
                uri = uri + "&modulo=BOLETOLOCAWEB";
                uri = uri + "&ambiente=PRODUCAO";
                uri = uri + "&valor=" + val;
                uri = uri + "&numdoc=" + OrderId.ToString();
                uri = uri + "&sacado=" + customer.Name;
                uri = uri + "&cgccpfsac=" + customer.SSN;
                uri = uri + "&enderecosac=" + customer.Address;
                uri = uri + "&numeroendsac=" + customer.Number;
                uri = uri + "&complementosac=" + customer.Number;
                uri = uri + "&bairrosac=" + customer.Neighborhood;
                uri = uri + "&cidadesac=" + customer.City;
                uri = uri + "&cepsac=" + customer.Zip;
                uri = uri + "&ufsac=" + customer.State;
                uri = uri + "&datadoc=" + DateTime.Now.ToString("dd/MM/yyyy");
                uri = uri + "&vencto=" + DateTime.Now.AddDays(DaysToDue).ToString("dd/MM/yyyy");
                uri = uri + "&instr1=" + Instructions;
                uri = uri + "&instr2=";
                uri = uri + "&instr3=";
                uri = uri + "&instr4=";
                uri = uri + "&instr5=";
                uri = uri + "&numdocespec=";
                uri = uri + "&nossonum=82" + order.CustomerId.ToString("00000") + OrderId.ToString("000000");
                uri = uri + "&cnab=";
                uri = uri + "&campolivreespec=";
                uri = uri + "&debug=";
                uri = uri + "&logoloja=" + urlLogo;
                uri = uri + "&tituloloja=" + merchant.Merchant;
                uri = uri + "&botoesboleto=1";
                uri = uri + "&urltopoloja=";
                uri = uri + "&cabecalho=0";
                StringBuilder sb = new StringBuilder();
                byte[] buf = new byte[8192];
                HttpWebRequest request = (HttpWebRequest)
                WebRequest.Create(uri);
                HttpWebResponse response = (HttpWebResponse)
                request.GetResponse();
                Stream resStream = response.GetResponseStream();
                string tempString = null;
                int count = 0;
                do
                {
                    count = resStream.Read(buf, 0, buf.Length);
                    if (count != 0)
                    {
                        tempString = Encoding.ASCII.GetString(buf, 0, count);
                        sb.Append(tempString);
                    }
                }
                while (count > 0); 
                ret = ret + sb.ToString();
            }
            return ret;
        }
        [WebMethod]
        public string GenerateBoleto(int MerchantId, string Password, int OrderId, int Rebate, string urlLogo, string Instructions, int DaysToDue)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var customer = SqlServer.spGetCustomer(order.CustomerId, MerchantId).Single();
            var merchant = SqlServer.spGetMerchant(MerchantId).Single();
            //string vencimento = string.Format("{0:dd/MM/yyyy}", DateTime.Today.AddDays(DaysToDue));
            DateTime vencimento = DateTime.Today.AddDays(DaysToDue);
            string p1 = string.Format("{0:f2}", order.FreightValue + order.ProductsSum);
            //string p2 = p1.Replace(".", ",");
            //string valorBoleto = p2;
            string valorBoleto = p1;
            string numeroDocumento = OrderId.ToString();

            //cedente
            string[] conta = merchant.Account.Split('-');
            string cedente_codigo = conta[0] + conta[1];
            string cedente_nossoNumeroBoleto = OrderId.ToString("00000000") ;
            string cedente_cpfCnpj = merchant.SscOrFtid;
            string cedente_nome = merchant.AccountName;
            string cedente_agencia =merchant.Agency;
            
            string cedente_conta = conta[0];
            string cedente_digitoConta = conta[1]; 

            //sacado
            string sacado_cpfCnpj = customer.SSN;
            string sacado_nome = customer.Name;
            string sacado_endereco = customer.Address;
            string sacado_bairro = customer.Neighborhood;
            string sacado_cidade = customer.City;
            string sacado_cep = customer.Zip;
            string sacado_uf = customer.State;
            
            Cedente cedente = new Cedente(cedente_cpfCnpj, cedente_nome, cedente_agencia, cedente_conta, cedente_digitoConta);
            cedente.Codigo = Convert.ToInt32(cedente_codigo);

            //Boleto boleto = new Boleto(Convert.ToDateTime(vencimento),Convert.ToDouble(valorBoleto),"109",cedente_nossoNumeroBoleto,cedente);
            Boleto boleto = new Boleto(vencimento, Convert.ToDouble(valorBoleto), "109", cedente_nossoNumeroBoleto, cedente);
            
            boleto.NumeroDocumento = numeroDocumento;

            Sacado sacado = new Sacado(sacado_cpfCnpj, sacado_nome);
            boleto.Sacado = sacado;
            boleto.Sacado.Endereco.End = sacado_endereco;
            boleto.Sacado.Endereco.Bairro = sacado_bairro;
            boleto.Sacado.Endereco.Cidade = sacado_cidade;
            boleto.Sacado.Endereco.CEP = sacado_cep;
            boleto.Sacado.Endereco.UF = sacado_uf;

            Instrucao_Itau instrucao = new Instrucao_Itau();
            instrucao.Descricao = "Não Receber após o vencimento";

            boleto.Instrucoes.Add(instrucao);
            EspecieDocumento_Itau especie = new EspecieDocumento_Itau(99);
            boleto.EspecieDocumento = especie;

            BoletoBancario boleto_bancario = new BoletoBancario();
            boleto_bancario.CodigoBanco = 341;
            boleto_bancario.Boleto = boleto;
            boleto_bancario.MostrarCodigoCarteira = true;
            boleto_bancario.Boleto.Valida();

            boleto_bancario.MostrarComprovanteEntrega = true;

            StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new System.IO.StringWriter(sb);
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                System.Web.UI.Page p = new System.Web.UI.Page();
                boleto_bancario.Page = p;
                boleto_bancario.ApplyStyleSheetSkin(p);
                boleto_bancario.RenderControl(hw);
                string ret = sb.ToString();
                ret =  ret.Replace("/WebResource", "http://couvax.marcojr.com.br/WebResource");
                ret = ret.Replace("ImagemCodigoBarra.ashx", "http://couvax.marcojr.com.br/ImagemCodigoBarra.ashx");
                string st = "<style type=~text/css~>";
                st = st + "body";
                st = st + "{";
                st = st + "	color:#000000;";
                st = st + "	background-color:#ffffff;";
                st = st + "	margin-top:0;";
                st = st + "	margin-right:0;";
                st = st + "}";
                st = st + "";
                st = st + "*{margin:0px;padding:0px}";
                st = st + "table{border:0;border-collapse:collapse;padding:0}";
                st = st + "";
                st = st + "img{border:0}";
                st = st + "";
                st = st + ".cp";
                st = st + "{";
                st = st + "	font: bold 10px arial;";
                st = st + "	color: black";
                st = st + "}";
                st = st + ".ti";
                st = st + "{";
                st = st + "	font: 9px arial, helvetica, sans-serif";
                st = st + "}";
                st = st + ".ld";
                st = st + "{";
                st = st + "	font: bold 15px arial;";
                st = st + "	color: #000000";
                st = st + "}";
                st = st + ".ct";
                st = st + "{";
                st = st + "	font: 9px ~arial narrow~;";
                st = st + "	color: #000033";
                st = st + "}";
                st = st + ".cn";
                st = st + "{";
                st = st + "	font: 9px arial;";
                st = st + "	color: black";
                st = st + "}";
                st = st + ".bc";
                st = st + "{";
                st = st + "	font: bold 22px arial;";
                st = st + "	color: #000000";
                st = st + "}";
                st = st + "";
                st = st + ".cut{width:665px;height:1px;border-top:dashed 1px #000}";
                st = st + ".Ac{text-align:center}";
                st = st + ".Ar{text-align:right}";
                st = st + ".Al{text-align:left}";
                st = st + ".At{vertical-align:top}";
                st = st + ".Ab{vertical-align:bottom}";
                st = st + ".ct td, .cp td{padding-left:6px;border-left:solid 1px #000}";
                st = st + ".cpN{font:bold 10px arial;color:black}";
                st = st + ".ctN{font:9px ~arial narrow~;color:#000033}";
                st = st + ".pL0{padding-left:0px}";
                st = st + ".pL6{padding-left:6px}";
                st = st + ".pL10{padding-left:10px}";
                st = st + ".imgLogo{width:150px}";
                st = st + ".imgLogo img{width:150px;height:40px}";
                st = st + ".barra{width:3px;height:22px;vertical-align:bottom}";
                st = st + ".barra img{width:2px;height:22px}";
                st = st + ".rBb td{border-bottom:solid 1px #000}";
                st = st + ".BB{border-bottom:solid 1px #000}";
                st = st + ".BL{border-left:solid 1px #000}";
                st = st + ".BR{border-right:solid 1px #000}";
                st = st + ".BT1{border-top:dashed 1px #000}";
                st = st + ".BT2{border-top:solid 2px #000}";
                st = st + ".h1{height:1px}";
                st = st + ".h13{height:13px}";
                st = st + ".h12{height:12px}";
                st = st + ".h13 td{vertical-align:top}";
                st = st + ".h12 td{vertical-align:top}";
                st = st + ".w6{width:6px}";
                st = st + ".w7{width:7px;}";
                st = st + ".w34{width:34px}";
                st = st + ".w45{width:45px}";
                st = st + ".w53{width:53px}";
                st = st + ".w62{width:62px}";
                st = st + ".w65{width:65px}";
                st = st + ".w72{width:72px}";
                st = st + ".w83{width:83px}";
                st = st + ".w88{width:88px}";
                st = st + ".w104{width:104px}";
                st = st + ".w105{width:105px}";
                st = st + ".w106{width:106px}";
                st = st + ".w113{width:113px}";
                st = st + ".w112{width:112px}";
                st = st + ".w123{width:123px}";
                st = st + ".w126{width:126px}";
                st = st + ".w128{width:128px}";
                st = st + ".w132{width:132px}";
                st = st + ".w134{width:134px}";
                st = st + ".w150{width:150px}";
                st = st + ".w163{width:163px}";
                st = st + ".w164{width:164px}";
                st = st + ".w180{width:180px}";
                st = st + ".w182{width:182px}";
                st = st + ".w186{width:186px}";
                st = st + ".w192{width:192px}";
                st = st + ".w250{width:250px}";
                st = st + ".w298{width:298px}";
                st = st + ".w409{width:409px}";
                st = st + ".w472{width:472px}";
                st = st + ".w478{width:478px}";
                st = st + ".w500{width:500px}";
                st = st + ".w544{width:544px}";
                st = st + ".w564{width:564px}";
                st = st + ".w659{width:659px}";
                st = st + ".w666{width:666px}";
                st = st + ".w667{width:667px}";
                st = st + ".BHead td{border-bottom:solid 2px #000}";
                st = st + ".EcdBar{height:50px;vertical-align:bottom}";
                st = st + ".rc6 td{vertical-align:top;border-bottom:solid 1px #000;border-left:solid 1px #000}";
                st = st + ".rc6 div{padding-left:6px}";
                st = st + ".rc6 .t{font:9px ~arial narrow~;color:#000033;height:13px}";
                st = st + ".rc6 .c{font:bold 10px arial;color:black;height:12px}";
                st = st + ".mt23{margin-top:23px;}";
                st = st + ".pb4{padding-bottom:14px;}";
                st = st + ".ebc{width:4px;height:440px;border-right:dotted 1px #000000;margin-right:4px;}</style>";
                ret = st.Replace('~', '"') + ret;
                ret = st.Replace('~', '"') + ret;
                return ret;
        }
        [WebMethod]
        public string GenerateBoleto_Old(int MerchantId, string Password,int OrderId, int Rebate, string urlLogo, string Instructions, int DaysToDue)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var customer = SqlServer.spGetCustomer(order.CustomerId,MerchantId).Single();
            var merchant = SqlServer.spGetMerchant(MerchantId).Single();
            string cpf_cnpj = merchant.SscOrFtid;
            cpf_cnpj = cpf_cnpj.Replace(".","");
            cpf_cnpj = cpf_cnpj.Replace("-", "");
            cpf_cnpj = cpf_cnpj.Replace("/", "");
            cpf_cnpj = cpf_cnpj.Replace("\\", "");

            Cedente c = new Cedente(merchant.SscOrFtid, merchant.Merchant +" (" + merchant.SscOrFtid +")", merchant.Agency,merchant.Account);
            string[] CodCedente = merchant.BankInternalId.Split('-');
            c.Codigo = System.Convert.ToInt32(CodCedente[0]);
            c.DigitoCedente = System.Convert.ToInt32(CodCedente[1]);
            string num = OrderId.ToString("0000000000");
            Boleto b = new Boleto(DateTime.Now.AddDays(DaysToDue), System.Convert.ToDouble(order.FreightValue + order.ProductsSum), merchant.ChargeTypeForBoleto.ToString(), num,c);
            Sacado s = new Sacado(customer.SSN, customer.Name);
            BoletoBancario bb = new BoletoBancario();
            b.NumeroDocumento = OrderId.ToString();
            s.Endereco.End = customer.Address;
            s.Endereco.Bairro = customer.Neighborhood;
            s.Endereco.Cidade = customer.City;
            s.Endereco.CEP = customer.Zip;
            s.Endereco.UF = customer.State;
            b.Sacado = s;
            string ret = "";
            if (merchant.Bank == 104)
            {
                Instrucao_Caixa i = new Instrucao_Caixa();
                i.Descricao = Instructions;
                b.Instrucoes.Add(i);
                b.EspecieDocumento = new EspecieDocumento_Caixa();
                bb.CodigoBanco = (short)merchant.Bank;
                bb.Boleto = b;
                bb.MostrarCodigoCarteira = true;
                bb.Boleto.Valida();
                bb.MostrarComprovanteEntrega = true;
                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new System.IO.StringWriter(sb);
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                System.Web.UI.Page p = new System.Web.UI.Page();
                bb.Page = p;
                bb.ApplyStyleSheetSkin(p);
                bb.RenderControl(hw);
                ret = sb.ToString();
                ret = ret.Replace("/WebResource", "http://couvax.marcojr.com.br/WebResource");
                ret = ret.Replace("ImagemCodigoBarra.ashx", "http://couvax.marcojr.com.br/ImagemCodigoBarra.ashx");
                string st = "<style type=~text/css~>";
                st = st + "body";
                st = st + "{";
                st = st + "	color:#000000;";
                st = st + "	background-color:#ffffff;";
                st = st + "	margin-top:0;";
                st = st + "	margin-right:0;";
                st = st + "}";
                st = st + "";
                st = st + "*{margin:0px;padding:0px}";
                st = st + "table{border:0;border-collapse:collapse;padding:0}";
                st = st + "";
                st = st + "img{border:0}";
                st = st + "";
                st = st + ".cp";
                st = st + "{";
                st = st + "	font: bold 10px arial;";
                st = st + "	color: black";
                st = st + "}";
                st = st + ".ti";
                st = st + "{";
                st = st + "	font: 9px arial, helvetica, sans-serif";
                st = st + "}";
                st = st + ".ld";
                st = st + "{";
                st = st + "	font: bold 15px arial;";
                st = st + "	color: #000000";
                st = st + "}";
                st = st + ".ct";
                st = st + "{";
                st = st + "	font: 9px ~arial narrow~;";
                st = st + "	color: #000033";
                st = st + "}";
                st = st + ".cn";
                st = st + "{";
                st = st + "	font: 9px arial;";
                st = st + "	color: black";
                st = st + "}";
                st = st + ".bc";
                st = st + "{";
                st = st + "	font: bold 22px arial;";
                st = st + "	color: #000000";
                st = st + "}";
                st = st + "";
                st = st + ".cut{width:665px;height:1px;border-top:dashed 1px #000}";
                st = st + ".Ac{text-align:center}";
                st = st + ".Ar{text-align:right}";
                st = st + ".Al{text-align:left}";
                st = st + ".At{vertical-align:top}";
                st = st + ".Ab{vertical-align:bottom}";
                st = st + ".ct td, .cp td{padding-left:6px;border-left:solid 1px #000}";
                st = st + ".cpN{font:bold 10px arial;color:black}";
                st = st + ".ctN{font:9px ~arial narrow~;color:#000033}";
                st = st + ".pL0{padding-left:0px}";
                st = st + ".pL6{padding-left:6px}";
                st = st + ".pL10{padding-left:10px}";
                st = st + ".imgLogo{width:150px}";
                st = st + ".imgLogo img{width:150px;height:40px}";
                st = st + ".barra{width:3px;height:22px;vertical-align:bottom}";
                st = st + ".barra img{width:2px;height:22px}";
                st = st + ".rBb td{border-bottom:solid 1px #000}";
                st = st + ".BB{border-bottom:solid 1px #000}";
                st = st + ".BL{border-left:solid 1px #000}";
                st = st + ".BR{border-right:solid 1px #000}";
                st = st + ".BT1{border-top:dashed 1px #000}";
                st = st + ".BT2{border-top:solid 2px #000}";
                st = st + ".h1{height:1px}";
                st = st + ".h13{height:13px}";
                st = st + ".h12{height:12px}";
                st = st + ".h13 td{vertical-align:top}";
                st = st + ".h12 td{vertical-align:top}";
                st = st + ".w6{width:6px}";
                st = st + ".w7{width:7px;}";
                st = st + ".w34{width:34px}";
                st = st + ".w45{width:45px}";
                st = st + ".w53{width:53px}";
                st = st + ".w62{width:62px}";
                st = st + ".w65{width:65px}";
                st = st + ".w72{width:72px}";
                st = st + ".w83{width:83px}";
                st = st + ".w88{width:88px}";
                st = st + ".w104{width:104px}";
                st = st + ".w105{width:105px}";
                st = st + ".w106{width:106px}";
                st = st + ".w113{width:113px}";
                st = st + ".w112{width:112px}";
                st = st + ".w123{width:123px}";
                st = st + ".w126{width:126px}";
                st = st + ".w128{width:128px}";
                st = st + ".w132{width:132px}";
                st = st + ".w134{width:134px}";
                st = st + ".w150{width:150px}";
                st = st + ".w163{width:163px}";
                st = st + ".w164{width:164px}";
                st = st + ".w180{width:180px}";
                st = st + ".w182{width:182px}";
                st = st + ".w186{width:186px}";
                st = st + ".w192{width:192px}";
                st = st + ".w250{width:250px}";
                st = st + ".w298{width:298px}";
                st = st + ".w409{width:409px}";
                st = st + ".w472{width:472px}";
                st = st + ".w478{width:478px}";
                st = st + ".w500{width:500px}";
                st = st + ".w544{width:544px}";
                st = st + ".w564{width:564px}";
                st = st + ".w659{width:659px}";
                st = st + ".w666{width:666px}";
                st = st + ".w667{width:667px}";
                st = st + ".BHead td{border-bottom:solid 2px #000}";
                st = st + ".EcdBar{height:50px;vertical-align:bottom}";
                st = st + ".rc6 td{vertical-align:top;border-bottom:solid 1px #000;border-left:solid 1px #000}";
                st = st + ".rc6 div{padding-left:6px}";
                st = st + ".rc6 .t{font:9px ~arial narrow~;color:#000033;height:13px}";
                st = st + ".rc6 .c{font:bold 10px arial;color:black;height:12px}";
                st = st + ".mt23{margin-top:23px;}";
                st = st + ".pb4{padding-bottom:14px;}";
                st = st + ".ebc{width:4px;height:440px;border-right:dotted 1px #000000;margin-right:4px;}</style>";
                ret = st.Replace('~','"') + ret;
            }
            
            if (merchant.Bank == 341)
            {
                Instrucao_Caixa i = new Instrucao_Caixa();
                i.Descricao = Instructions;
                b.Instrucoes.Add(i);
                b.EspecieDocumento = new EspecieDocumento_Caixa();
                bb.CodigoBanco = (short)merchant.Bank;
                bb.Boleto = b;
                bb.MostrarCodigoCarteira = true;
                bb.Boleto.Valida();
                bb.MostrarComprovanteEntrega = true;
                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new System.IO.StringWriter(sb);
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                System.Web.UI.Page p = new System.Web.UI.Page();
                bb.Page = p;
                bb.ApplyStyleSheetSkin(p);
                bb.RenderControl(hw);
                ret = sb.ToString();
                ret = ret.Replace("/WebResource", "http://couvax.marcojr.com.br/WebResource");
                ret = ret.Replace("ImagemCodigoBarra.ashx", "http://couvax.marcojr.com.br/ImagemCodigoBarra.ashx");
                string st = "<style type=~text/css~>";
                st = st + "body";
                st = st + "{";
                st = st + "	color:#000000;";
                st = st + "	background-color:#ffffff;";
                st = st + "	margin-top:0;";
                st = st + "	margin-right:0;";
                st = st + "}";
                st = st + "";
                st = st + "*{margin:0px;padding:0px}";
                st = st + "table{border:0;border-collapse:collapse;padding:0}";
                st = st + "";
                st = st + "img{border:0}";
                st = st + "";
                st = st + ".cp";
                st = st + "{";
                st = st + "	font: bold 10px arial;";
                st = st + "	color: black";
                st = st + "}";
                st = st + ".ti";
                st = st + "{";
                st = st + "	font: 9px arial, helvetica, sans-serif";
                st = st + "}";
                st = st + ".ld";
                st = st + "{";
                st = st + "	font: bold 15px arial;";
                st = st + "	color: #000000";
                st = st + "}";
                st = st + ".ct";
                st = st + "{";
                st = st + "	font: 9px ~arial narrow~;";
                st = st + "	color: #000033";
                st = st + "}";
                st = st + ".cn";
                st = st + "{";
                st = st + "	font: 9px arial;";
                st = st + "	color: black";
                st = st + "}";
                st = st + ".bc";
                st = st + "{";
                st = st + "	font: bold 22px arial;";
                st = st + "	color: #000000";
                st = st + "}";
                st = st + "";
                st = st + ".cut{width:665px;height:1px;border-top:dashed 1px #000}";
                st = st + ".Ac{text-align:center}";
                st = st + ".Ar{text-align:right}";
                st = st + ".Al{text-align:left}";
                st = st + ".At{vertical-align:top}";
                st = st + ".Ab{vertical-align:bottom}";
                st = st + ".ct td, .cp td{padding-left:6px;border-left:solid 1px #000}";
                st = st + ".cpN{font:bold 10px arial;color:black}";
                st = st + ".ctN{font:9px ~arial narrow~;color:#000033}";
                st = st + ".pL0{padding-left:0px}";
                st = st + ".pL6{padding-left:6px}";
                st = st + ".pL10{padding-left:10px}";
                st = st + ".imgLogo{width:150px}";
                st = st + ".imgLogo img{width:150px;height:40px}";
                st = st + ".barra{width:3px;height:22px;vertical-align:bottom}";
                st = st + ".barra img{width:2px;height:22px}";
                st = st + ".rBb td{border-bottom:solid 1px #000}";
                st = st + ".BB{border-bottom:solid 1px #000}";
                st = st + ".BL{border-left:solid 1px #000}";
                st = st + ".BR{border-right:solid 1px #000}";
                st = st + ".BT1{border-top:dashed 1px #000}";
                st = st + ".BT2{border-top:solid 2px #000}";
                st = st + ".h1{height:1px}";
                st = st + ".h13{height:13px}";
                st = st + ".h12{height:12px}";
                st = st + ".h13 td{vertical-align:top}";
                st = st + ".h12 td{vertical-align:top}";
                st = st + ".w6{width:6px}";
                st = st + ".w7{width:7px;}";
                st = st + ".w34{width:34px}";
                st = st + ".w45{width:45px}";
                st = st + ".w53{width:53px}";
                st = st + ".w62{width:62px}";
                st = st + ".w65{width:65px}";
                st = st + ".w72{width:72px}";
                st = st + ".w83{width:83px}";
                st = st + ".w88{width:88px}";
                st = st + ".w104{width:104px}";
                st = st + ".w105{width:105px}";
                st = st + ".w106{width:106px}";
                st = st + ".w113{width:113px}";
                st = st + ".w112{width:112px}";
                st = st + ".w123{width:123px}";
                st = st + ".w126{width:126px}";
                st = st + ".w128{width:128px}";
                st = st + ".w132{width:132px}";
                st = st + ".w134{width:134px}";
                st = st + ".w150{width:150px}";
                st = st + ".w163{width:163px}";
                st = st + ".w164{width:164px}";
                st = st + ".w180{width:180px}";
                st = st + ".w182{width:182px}";
                st = st + ".w186{width:186px}";
                st = st + ".w192{width:192px}";
                st = st + ".w250{width:250px}";
                st = st + ".w298{width:298px}";
                st = st + ".w409{width:409px}";
                st = st + ".w472{width:472px}";
                st = st + ".w478{width:478px}";
                st = st + ".w500{width:500px}";
                st = st + ".w544{width:544px}";
                st = st + ".w564{width:564px}";
                st = st + ".w659{width:659px}";
                st = st + ".w666{width:666px}";
                st = st + ".w667{width:667px}";
                st = st + ".BHead td{border-bottom:solid 2px #000}";
                st = st + ".EcdBar{height:50px;vertical-align:bottom}";
                st = st + ".rc6 td{vertical-align:top;border-bottom:solid 1px #000;border-left:solid 1px #000}";
                st = st + ".rc6 div{padding-left:6px}";
                st = st + ".rc6 .t{font:9px ~arial narrow~;color:#000033;height:13px}";
                st = st + ".rc6 .c{font:bold 10px arial;color:black;height:12px}";
                st = st + ".mt23{margin-top:23px;}";
                st = st + ".pb4{padding-bottom:14px;}";
                st = st + ".ebc{width:4px;height:440px;border-right:dotted 1px #000000;margin-right:4px;}</style>";
                ret = st.Replace('~', '"') + ret;
            }
            return ret;
        }
        [WebMethod]
        public string Authorize(int MerchantId, string Password, int Pmg, string CardNumber, string CardHolder, int ExpMonth, int ExpYear, int CVC, int OrderId, string ReturnURL, bool UseMerchantPage, bool Testing, string Acquirer)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            if (Pmg == 1) { throw new UnauthorizedAccessException("Access is Denied"); }
            string r = "";
            string ret = "";
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var cus = SqlServer.spGetCustomer(order.CustomerId,MerchantId).Single();
            var p = (from c in SqlServer.LOJA_PMGs
            where c.ID_LOJA.Equals(MerchantId)
            where c.ID_PMG.Equals(Pmg)
            select c).Single();
            decimal balance = 0;
            if (cus.Balance != null)
            {
                balance = (decimal)cus.Balance;
            }
            decimal amount = (decimal)order.ProductsSum + (decimal)order.FreightValue - balance;
            SqlServer.spSetPmgOnOrder(MerchantId, OrderId, Pmg);
            if (Pmg == 1)
            {
                r = DoCieloAuth(
                                p.CONTA, p.TOKEN,
                                p.PRODUTO.ToString(),
                                OrderId.ToString(),
                                amount,
                                order.PaymentType.ToLower(),
                                order.Installments.ToString(),
                                ReturnURL,
                                order.Token.ToString()
                              );
                ret = r;
                int tid_start = r.IndexOf("<tid>");
                int erro_start = r.IndexOf("<erro");
                if (erro_start > 0)
                {
                    //Error - Will Retry
                    r = DoCieloAuth(
                                p.CONTA, p.TOKEN,
                                p.PRODUTO.ToString(),
                                OrderId.ToString(),
                                System.Convert.ToDecimal(amount),
                                order.PaymentType.ToLower(),
                                order.Installments.ToString(),
                                ReturnURL,
                                order.Token.ToString()
                              );
                    ret = r;
                    tid_start = r.IndexOf("<tid>");
                    erro_start = r.IndexOf("<erro");
                }
                if (erro_start > 0)
                {
                    ret = "Error:" + r;
                }
                if (tid_start > -1)
                {
                    tid_start = tid_start + 5;
                    int tid_end = r.IndexOf("</tid>");
                    string tid = r.Substring(tid_start, tid_end - tid_start);
                    SqlServer.spSetOrderTid(MerchantId, OrderId, tid);
                }
                int url_start = r.IndexOf("<url-autenticacao>");
                if (url_start > -1)
                {
                    url_start = url_start + 18;
                    int url_end = r.IndexOf("</url-autenticacao>");
                    string url = r.Substring(url_start, url_end - url_start);
                    ret = url;
                }
            }
            if (Pmg == 3)
            {
                string url = "https://www.aprovafacil.com/cgi-bin/APFW/" + p.CONTA +"/APC";
                if (Testing)
                {
                    url = "https://teste.aprovafacil.com/cgi-bin/APFW/" + p.CONTA + "/APC";
                }
                
                try
                {
                    r = DoCobreBemAuth(MerchantId, url, OrderId, CardNumber, ExpMonth, ExpYear, CVC, Acquirer);
                    ret = r;
                    int res_start = r.IndexOf("<TransacaoAprovada>");
                    int res_end = r.IndexOf("</TransacaoAprovada>");
                    int tid_start = r.IndexOf("<Transacao>");
                    int tid_end = r.IndexOf("</Transacao>");
                    int res_acquirer_start = r.IndexOf("<ResultadoSolicitacaoAprovacao>");
                    int res_acquirer_end = r.IndexOf("</ResultadoSolicitacaoAprovacao>");
                    string tid = r.Substring((tid_start + "<Transacao>".Length), tid_end - (tid_start + "<Transacao>".Length));
                    if (tid.Length > 3)
                    {
                        SqlServer.spSetOrderTid(MerchantId, OrderId, tid);
                    }
                    string res_app = r.Substring((res_start + "<TransacaoAprovada>".Length), res_end - (res_start + "<TransacaoAprovada>".Length));
                    string res_acq = r.Substring((res_acquirer_start + "<ResultadoSolicitacaoAprovacao>".Length), res_acquirer_end - (res_acquirer_start + "<ResultadoSolicitacaoAprovacao>".Length));
                    if (res_app.ToUpper().Trim() == "TRUE")
                    {
                        var merchant = SqlServer.spGetMerchant(MerchantId).Single();
                        if (merchant.MinimumOrderToCheckFraud > System.Convert.ToInt32(order.ProductsSum + order.FreightValue))
                        {
                            SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AAC");
                        }
                        else
                        {
                            SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AFC");
                            internal_antifraud.Antifraud a = new internal_antifraud.Antifraud();
                            a.SendToAnalysisQueue(MerchantId, Password, (int)merchant.AntiFraudProvider, OrderId);
                        }
                    }
                    else
                    {
                        SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "REF");
                    }
                }
                catch {
                    ret = "Error:" + r;
                }
            }
            if (Pmg == 1006)
            {
                string mode = "LIVE";
                string url = "https://ctpe.net/frontend/payment.prc";
                //url = " https://test.ctpe.net/frontend/payment.prc";
                //mode = "CONNECTOR_TEST";
                if (Testing)
                {
                    url = " https://test.ctpe.net/frontend/payment.prc";
                    mode = "INTEGRATOR_TEST";
                }
                try
                {
		            string channel = "";
                    if(Acquirer.ToLower() == "cielo")
                    {
                        channel = "8a8394c1389f797e0138a44946740bd9";
                    }		
                    r = DoAllPagoAuth(MerchantId, url, OrderId, CardNumber,CardHolder, ExpMonth, ExpYear, CVC, channel,p.CONTA, p.SENHA,p.TOKEN,mode);
                    ret = r;
                    string[] pars = ret.Split('&');
                    for (int i = 0; i < pars.Length; i++)
                    {
                        string[] data = pars[i].Split('=');
                        string field = data[0];
                        string value = data[1];
                        if (field == "PROCESSING.CONNECTORDETAIL.ConnectorTxID1")
                        {
                            SqlServer.spSetOrderTid(MerchantId, OrderId, value);
                        }
                        if (field == "IDENTIFICATION.UNIQUEID")
                        {
                            SqlServer.spSetOrderTidGateway(MerchantId, OrderId, value);
                        }
                        if (field == "PROCESSING.REASON.CODE")
                        {
                            SqlServer.spUpdateOrderPMGstatus(MerchantId, OrderId, Convert.ToInt16(value));
                        }
                        if (field == "PROCESSING.RESULT")
                        {
                            if (value == "ACK")
                            {
                                SqlServer.spIncreaseCustomerBalance(MerchantId, order.CustomerId, balance * -1);
                                var merchant = SqlServer.spGetMerchant(MerchantId).Single();
                                if (merchant.MinimumOrderToCheckFraud > System.Convert.ToInt32(order.ProductsSum + order.FreightValue))
                                {
                                    SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AAC");
                                }
                                else
                                {
                                    SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AFC");
                                    internal_antifraud.Antifraud a = new internal_antifraud.Antifraud();
                                    a.SendToAnalysisQueue(MerchantId, Password, (int)merchant.AntiFraudProvider, OrderId);
                                }
                            }
                            else
                            {
                                SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "REF");
                            }
                        }
                    }
                }
                catch {
                    ret = "Error:" + r;
                }
            }
            return ret;
        }
        [WebMethod]
        public string Capture(int MerchantId, string Password, int OrderId, int Pmg, bool Testing)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            if (Pmg == 1) { throw new UnauthorizedAccessException("Access is Denied"); }
            string r = "";
            string ret = "";
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            if (Pmg == 1)
            {
                var p = (from c in SqlServer.LOJA_PMGs
                         where c.ID_LOJA.Equals(MerchantId)
                         where c.ID_PMG.Equals(1)
                         select c).Single();
                r = DoCieloCapture(p.CONTA, p.TOKEN,order.TransactionId,OrderId.ToString());
                ret = r;
                int tid_start = r.IndexOf("<tid>");
                int erro_start = r.IndexOf("<erro");
                if (erro_start > 0)
                {
                    //Error - Will Retry
                    r = DoCieloCapture(p.CONTA, p.TOKEN, order.TransactionId, OrderId.ToString());
                    ret = r;
                    erro_start = r.IndexOf("<erro");
                }
                if (erro_start > 0)
                {
                    ret = "Error:" + r;
                }
                else
                {
                    SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "CAP");
                }
            }
            if (Pmg == 3)
            {
                var p = (from c in SqlServer.LOJA_PMGs
                         where c.ID_LOJA.Equals(MerchantId)
                         where c.ID_PMG.Equals(Pmg)
                         select c).Single();
                string url = "https://www.aprovafacil.com/cgi-bin/APFW/" + p.CONTA + "/APC";
                if (Testing)
                {
                    url = "https://teste.aprovafacil.com/cgi-bin/APFW/" + p.CONTA + "/APC";
                }
                r = DoCobreBemCapture(MerchantId,OrderId, url);
                int conf_start = r.IndexOf("Confirmado");
                int erro_start = r.IndexOf("Erro");
                if (conf_start > 0)
                {
                    SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "CAP");
                }
                if (erro_start > 0)
                {
                    ret = "Error:" + r;
                }
            }
            if (Pmg == 1006)
            {
                var p = (from c in SqlServer.LOJA_PMGs
                         where c.ID_LOJA.Equals(MerchantId)
                         where c.ID_PMG.Equals(Pmg)
                         select c).Single();
                string mode = "LIVE";
                string url = "https://ctpe.net/frontend/payment.prc";
                if (Testing)
                {
                    url = " https://test.ctpe.net/frontend/payment.prc";
                    mode = "INTEGRATOR_TEST";
                }
                r = DoAllPagoCapture(MerchantId, url, OrderId, "8a8394c1389f797e0138a44946740bd9", p.CONTA, p.SENHA, p.TOKEN, mode);
                ret = r;
                string[] pars = ret.Split('&');
                for (int i = 0; i < pars.Length; i++)
                {
                    string[] data = pars[i].Split('=');
                    string field = data[0];
                    string value = data[1];
                    if (field == "PROCESSING.RESULT")
                    {
                        if (value == "ACK")
                        {
                            SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "CAP");
                        }
                        else
                        {
                            SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "ERR");
                        }
                    }
                }
            }
            return ret;
        }
        [WebMethod]
        public void UpdatePmgStatus(int MerchantId, string Password, int Pmg, int OrderId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            string r = "";
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var merchant = SqlServer.spGetMerchant(MerchantId).Single();
            if (Pmg == 1)
            {
                var p = (from c in SqlServer.LOJA_PMGs
                         where c.ID_LOJA.Equals(MerchantId)
                         where c.ID_PMG.Equals(Pmg)
                         select c).Single();
                r = QueryCieloTid(p.CONTA, p.TOKEN, order.TransactionId,OrderId.ToString()).ToString();
                int status_start = r.IndexOf("<status>");
                if (status_start > -1)
                {
                    status_start = status_start + 8;
                    int status_end = r.IndexOf("</status>");
                    string status = r.Substring(status_start, status_end - status_start);
                    SqlServer.spUpdateOrderPMGstatus(MerchantId, OrderId, System.Convert.ToInt32(status));
                    if (status.Trim() == "4")
                    {
                        if (merchant.MinimumOrderToCheckFraud > System.Convert.ToInt32(order.ProductsSum + order.FreightValue))
                        {
                            SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AAC");
                        }
                        else
                        {
                            SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "AFC");
                            internal_antifraud.Antifraud a = new internal_antifraud.Antifraud();
                            a.SendToAnalysisQueue(MerchantId, Password, (int)merchant.AntiFraudProvider, OrderId);
                        }
                    }
                    if (status.Trim() == "5")
                    {
                        SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "REF");
                    }
                    if (status.Trim() == "6")
                    {
                        SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "APP");
                    }
                    if (status.Trim() == "9")
                    {
                        SqlServer.spUpdateOrderStatus(MerchantId, OrderId, "CAN");
                    }
                }
            }
        }
        private string DoAllPagoCapture(int MerchantId, string Url, int OrderId, string Channel, string Login, string Password, string Sender, string Mode)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var cus = SqlServer.spGetCustomer(order.CustomerId, MerchantId).Single();
            string post = "";
            string r = "";
            decimal total = (decimal)order.FreightValue + (decimal)order.ProductsSum;
            string tf = total.ToString("#0.00");
            tf = tf.Replace(",", ".");
            post = post + "USER.LOGIN=" + Login + "&";
            post = post + "USER.PWD=" + Password + "&";
            post = post + "SECURITY.SENDER=" + Sender + "&";
            post = post + "TRANSACTION.MODE=" + Mode + "&";
            post = post + "TRANSACTION.RESPONSE=SYNC&";
            post = post + "TRANSACTION.CHANNEL=" + Channel + "&";
            post = post + "IDENTIFICATION.TRANSACTIONID=" + OrderId.ToString() + "&";
            post = post + "IDENTIFICATION.REFERENCEID=" + order.PmgTransactionId + "&";
            post = post + "PAYMENT.CODE=CC.CP&";
            post = post + "PRESENTATION.AMOUNT=" + tf + "&";
            post = post + "PRESENTATION.CURRENCY=BRL&";
            post = post + "PRESENTATION.USAGE=Pedido #" + OrderId.ToString() + "&";
            post = post + "CRITERION.CUSTOMERID=" + order.CustomerId.ToString();
            TextWriter tw = new StreamWriter("c:\\temp\\last_allpago_cap.txt");
            tw.WriteLine(post);
            tw.Close();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(post);
            System.Net.HttpWebRequest myRequest =
              (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Url);
            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            myRequest.ContentLength = data.Length;
            System.IO.Stream newStream = myRequest.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            System.Net.WebResponse response = myRequest.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            System.Text.Encoding myencoding = System.Text.Encoding.Default;
            System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
            Char[] charBuffer = new Char[256];
            int count = reader.Read(charBuffer, 0, charBuffer.Length);
            StringBuilder Dados = new StringBuilder();
            while (count > 0)
            {
                Dados.Append(new String(charBuffer, 0, count));
                count = reader.Read(charBuffer, 0, charBuffer.Length);
            }
            r = Dados.ToString();
            return r;
        }
        private string DoCieloAuth(string Account, string Token, string Product, string OrderId, decimal Amount, string CardType, string Installments, string ReturnBackUrl, string OrderToken)
        {
            string r = "";
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = "mensagem=<?xml version=~1.0~ encoding=~ISO-8859-1~?>";
                postData = postData + "<requisicao-transacao id=~" + OrderId + "~ versao=~1.1.0~>";
                postData = postData + "	<dados-ec>";
                postData = postData + "		<numero>" + Account + "</numero>";
                postData = postData + "		<chave>";
                postData = postData + "			" + Token;
                postData = postData + "		</chave>";
                postData = postData + "	</dados-ec>";
                postData = postData + "	<dados-pedido>";
                postData = postData + "		<numero>" + OrderId + "</numero>";
                postData = postData + "		<valor>" + Amount.ToString("#0.00").Replace(".", "").Replace(",", "") + "</valor>";
                postData = postData + "		<moeda>986</moeda>";
                postData = postData + "		<data-hora>#YYYY#-#MO#-#DD#T#HH#:#MI#:#SS#</data-hora>";
                postData = postData + "		<idioma>PT</idioma>";
                postData = postData + "	</dados-pedido>";
                postData = postData + "	<forma-pagamento>";
                postData = postData + "		<bandeira>" + CardType + "</bandeira>";
                if (System.Convert.ToInt16(Installments) > 1)
                {
                    postData = postData + "		<produto>" + Product + "</produto>";
                }
                else
                {
                    postData = postData + "		<produto>1</produto>";
                }
                postData = postData + "		<parcelas>" + Installments + "</parcelas>";
                postData = postData + "	</forma-pagamento>";
                postData = postData + "	<url-retorno>" + ReturnBackUrl + "</url-retorno>";
                postData = postData + "	<autorizar>2</autorizar>";
                postData = postData + "	<capturar>false</capturar>";
                postData = postData + "	<campo-livre>" + OrderToken +"</campo-livre>";
                postData = postData + "</requisicao-transacao>";
                postData = postData.Replace('~', '"');
                postData = postData.Replace("#YYYY#", DateTime.Now.Year.ToString("0000"));
                postData = postData.Replace("#MO#", DateTime.Now.Month.ToString("00"));
                postData = postData.Replace("#DD#", DateTime.Now.Day.ToString("00"));
                postData = postData.Replace("#HH#", DateTime.Now.Hour.ToString("00"));
                postData = postData.Replace("#MI#", DateTime.Now.Minute.ToString("00"));
                postData = postData.Replace("#SS#", DateTime.Now.Second.ToString("00"));
                byte[] data = encoding.GetBytes(postData);
                // Prepare web request...
                System.Net.HttpWebRequest myRequest =
                  (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://ecommerce.cielo.com.br/servicos/ecommwsec.do");
                myRequest.Method = "POST";
                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = data.Length;
                System.IO.Stream newStream = myRequest.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                System.Net.WebResponse response = myRequest.GetResponse();
                System.IO.Stream responseStream = response.GetResponseStream();
                System.Text.Encoding myencoding = System.Text.Encoding.Default;
                System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
                Char[] charBuffer = new Char[256];
                int count = reader.Read(charBuffer, 0, charBuffer.Length);
                StringBuilder Dados = new StringBuilder();
                while (count > 0)
                {
                    Dados.Append(new String(charBuffer, 0, count));
                    count = reader.Read(charBuffer, 0, charBuffer.Length);
                }
                r = Dados.ToString();
            }
            catch (Exception e)
            {
                r = "<error>Unable to connect</error>";
            }
            return r;
        }
        private string QueryCieloTid(string Account, string Token, string Tid, string OrderId)
        {
            string r = "";
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = "mensagem=<?xml version=~1.0~ encoding=~ISO-8859-1~?>";
                postData = postData + "<requisicao-consulta id=~" + OrderId + "~ versao=~1.1.0~>";
                postData = postData + "	<tid>";
                postData = postData + Tid;
                postData = postData + "	</tid>";
                postData = postData + "	<dados-ec>";
                postData = postData + "		<numero>" + Account + "</numero>";
                postData = postData + "		<chave>";
                postData = postData + "			" + Token;
                postData = postData + "		</chave>";
                postData = postData + "	</dados-ec>";
                postData = postData + "</requisicao-consulta>";
                postData = postData.Replace('~', '"');
                byte[] data = encoding.GetBytes(postData);
                // Prepare web request...
                System.Net.HttpWebRequest myRequest =
                  (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://ecommerce.cielo.com.br/servicos/ecommwsec.do");
                myRequest.Method = "POST";
                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = data.Length;
                System.IO.Stream newStream = myRequest.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                System.Net.WebResponse response = myRequest.GetResponse();
                System.IO.Stream responseStream = response.GetResponseStream();
                System.Text.Encoding myencoding = System.Text.Encoding.Default;
                System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
                Char[] charBuffer = new Char[256];
                int count = reader.Read(charBuffer, 0, charBuffer.Length);
                StringBuilder Dados = new StringBuilder();
                while (count > 0)
                {
                    Dados.Append(new String(charBuffer, 0, count));
                    count = reader.Read(charBuffer, 0, charBuffer.Length);
                }
                r = Dados.ToString();
            }
            catch (Exception e)
            {
                r = "<error>Unable to connect</error>";
            }
            return r;
        }
        public string DoCieloCapture(string Account, string Token, string Tid, string OrderId)
        {
            string r = "";
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = "mensagem=<?xml version=~1.0~ encoding=~ISO-8859-1~?>";
                postData = postData + "<requisicao-captura id=~" + OrderId + "~ versao=~1.1.1~>";
                postData = postData + "	<tid>";
                postData = postData + Tid;
                postData = postData + "	</tid>";
                postData = postData + "	<dados-ec>";
                postData = postData + "		<numero>" + Account + "</numero>";
                postData = postData + "		<chave>";
                postData = postData + "			" + Token;
                postData = postData + "		</chave>";
                postData = postData + "	</dados-ec>";
                postData = postData + "</requisicao-captura>";
                postData = postData.Replace('~', '"');
                byte[] data = encoding.GetBytes(postData);
                // Prepare web request...
                System.Net.HttpWebRequest myRequest =
                  (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://ecommerce.cielo.com.br/servicos/ecommwsec.do");
                myRequest.Method = "POST";
                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = data.Length;
                System.IO.Stream newStream = myRequest.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                System.Net.WebResponse response = myRequest.GetResponse();
                System.IO.Stream responseStream = response.GetResponseStream();
                System.Text.Encoding myencoding = System.Text.Encoding.Default;
                System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
                Char[] charBuffer = new Char[256];
                int count = reader.Read(charBuffer, 0, charBuffer.Length);
                StringBuilder Dados = new StringBuilder();
                while (count > 0)
                {
                    Dados.Append(new String(charBuffer, 0, count));
                    count = reader.Read(charBuffer, 0, charBuffer.Length);
                }
                r = Dados.ToString();
            }
            catch (Exception e)
            {
                r = "<error>Unable to connect</error>";
            }
            return r;
        }
        private string DoCobreBemAuth(int MerchantId, string Url, int OrderId, string CardNumber, int Month, int Year, int cvc, string Acquirer)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var cus = SqlServer.spGetCustomer(order.CustomerId,MerchantId).Single();
            string post = "";
            string r = "";
            decimal total = (decimal)order.FreightValue + (decimal)order.ProductsSum;
            string tf = total.ToString("#0.00");
            tf = tf.Replace(",", ".");
            post = post + "NumeroDocumento=" + OrderId.ToString() + "&";
            post = post + "ValorDocumento=" + tf + "&";
            post = post + "QuantidadeParcelas=" + order.Installments.ToString("00") + "&";
            post = post + "NumeroCartao=" + CardNumber + "&";
            post = post + "MesValidade=" + Month.ToString("00") + "&";
            post = post + "AnoValidade=" + Year.ToString() + "&";
            post = post + "CodigoSeguranca=" + cvc.ToString() + "&";
            post = post + "CPFPortador=" + cus.SSN + "&";
            post = post + "Adquirente=" + Acquirer + "&";
            post = post + "EnderecoIPComprador=" + order.IpAddress + "&";
            post = post + "PreAutorizacao=S";

            //post = post.Replace('~', '"');
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(post);
            System.Net.HttpWebRequest myRequest =
              (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Url);
            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded";
            myRequest.ContentLength = data.Length;
            System.IO.Stream newStream = myRequest.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            System.Net.WebResponse response = myRequest.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            System.Text.Encoding myencoding = System.Text.Encoding.Default;
            System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
            Char[] charBuffer = new Char[256];
            int count = reader.Read(charBuffer, 0, charBuffer.Length);
            StringBuilder Dados = new StringBuilder();
            while (count > 0)
            {
                Dados.Append(new String(charBuffer, 0, count));
                count = reader.Read(charBuffer, 0, charBuffer.Length);
            }
            r = Dados.ToString();
            return r;
        }
        private string DoCobreBemCapture(int MerchantId, int OrderId, string Url)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var cus = SqlServer.spGetCustomer(order.CustomerId, MerchantId).Single();
            string post = "";
            string r = "";
            decimal total = (decimal)order.FreightValue + (decimal)order.ProductsSum;
            post = post + "<input type=~text~ name~NumeroDocumento~ value=~" + OrderId.ToString() + "~>";
            post = post + "<input type=~text~ name~ValorDocumento~ value=~" + total.ToString("#0.00").Replace(',', '.') + "~>";
            post = post + "<input type=~text~ name~Transacao~ value=~" + order.TransactionId.ToString() + "~>";
            post = post.Replace('~', '"');
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(post);
            System.Net.HttpWebRequest myRequest =
              (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Url);
            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded";
            myRequest.ContentLength = data.Length;
            System.IO.Stream newStream = myRequest.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            System.Net.WebResponse response = myRequest.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            System.Text.Encoding myencoding = System.Text.Encoding.Default;
            System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
            Char[] charBuffer = new Char[256];
            int count = reader.Read(charBuffer, 0, charBuffer.Length);
            StringBuilder Dados = new StringBuilder();
            while (count > 0)
            {
                Dados.Append(new String(charBuffer, 0, count));
                count = reader.Read(charBuffer, 0, charBuffer.Length);
            }
            r = Dados.ToString();
            return r;
        }
        private string DoAllPagoAuth(int MerchantId, string Url, int OrderId, string CardNumber, string CardHolder, int Month, int Year, int cvc, string Channel, string Login, string Password, string Sender, string Mode)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var order = SqlServer.spGetOrder(MerchantId, OrderId).Single();
            var cus = SqlServer.spGetCustomer(order.CustomerId, MerchantId).Single();
            decimal balance = 0;
            string f_cvc = cvc.ToString();
            if (f_cvc.Length == 2)
            {
                f_cvc = "0" + f_cvc;
            }
            if (f_cvc.Length == 1)
            {
                f_cvc = "00" + f_cvc;
            }
            if (cus.Balance != null)
            {
                balance = (decimal)cus.Balance;
            }
            string post = "";
            string r = "";
            decimal total = (decimal)order.FreightValue + (decimal)order.ProductsSum - balance;
            string tf = total.ToString("#0.00");
            tf = tf.Replace(",", ".");
            string [] name = CardHolder.Split(' ');
            string first = name[0];
            string last = "";
            for(int i=1; i < name.Length; i++)
            {
                last = last + name[i] +" ";
            }
            last = last.Trim();
            post = post + "REQUEST.VERSION=1.0&";
            post = post + "SECURITY.SENDER=" + Sender +"&"; 
            post = post + "USER.LOGIN=" + Login +"&";
            post = post + "USER.PWD=" + Password +"&";
            post = post + "TRANSACTION.MODE=" + Mode +"&"; 
            post = post + "TRANSACTION.RESPONSE=SYNC&"; 
            post = post + "TRANSACTION.CHANNEL=" + Channel +"&"; 
            post = post + "IDENTIFICATION.TRANSACTIONID=" + OrderId.ToString() +"&"; 
            post = post + "PAYMENT.CODE=CC.PA&"; 
            post = post + "PRESENTATION.AMOUNT=" + tf +"&"; 
            post = post + "PRESENTATION.CURRENCY=BRL&"; 
            post = post + "PRESENTATION.USAGE=Pedido #" + OrderId.ToString() +"&"; 
            post = post + "ACCOUNT.HOLDER=" + CardHolder +"&"; 
            post = post + "ACCOUNT.NUMBER=" + CardNumber +"&";
            if (order.PaymentType.ToLower() != "visa")
            {
                post = post + "ACCOUNT.BRAND=MASTER&";
            }
            else
            {
                post = post + "ACCOUNT.BRAND=" + order.PaymentType + "&";
            }
            post = post + "ACCOUNT.EXPIRY_MONTH=" + Month +"&"; 
            post = post + "ACCOUNT.EXPIRY_YEAR=" + Year +"&"; 
            post = post + "ACCOUNT.VERIFICATION=" + f_cvc +"&"; 
            post = post + "NAME.GIVEN=" + first +"&"; 
            post = post + "NAME.FAMILY=" + last +"&"; 
            post = post + "ADDRESS.STREET=" + cus.Number +"," + cus.Address +" - " + cus.Complement +"&"; 
            post = post + "ADDRESS.ZIP=" + cus.Zip +"&"; 
            post = post + "ADDRESS.CITY=" + cus.City +"&"; 
            post = post + "ADDRESS.STATE=" + cus.State +"&"; 
            post = post + "ADDRESS.COUNTRY=BR&"; 
            post = post + "CONTACT.EMAIL=" + cus.Email +"&"; 
            post = post + "CONTACT.IP=" + order.IpAddress +"&";
            post = post + "CRITERION.CUSTOM_number_of_installments=" + order.Installments.ToString() + "&";
            post = post + "CRITERION.CUSTOMERID=" + order.CustomerId.ToString();
            TextWriter tw = new StreamWriter("c:\\temp\\last_allpago.txt");
            tw.WriteLine(post);
            tw.Close();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(post);
            System.Net.HttpWebRequest myRequest =
              (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Url);
            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            myRequest.ContentLength = data.Length;
            System.IO.Stream newStream = myRequest.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            System.Net.WebResponse response = myRequest.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            System.Text.Encoding myencoding = System.Text.Encoding.Default;
            System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, myencoding);
            Char[] charBuffer = new Char[256];
            int count = reader.Read(charBuffer, 0, charBuffer.Length);
            StringBuilder Dados = new StringBuilder();
            while (count > 0)
            {
                Dados.Append(new String(charBuffer, 0, count));
                count = reader.Read(charBuffer, 0, charBuffer.Length);
            }
            r = Dados.ToString();
            return r;
        }
    }
}
