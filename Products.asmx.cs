﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Globalization;

namespace Couvax
{
    /// <summary>
    /// Summary description for Products
    /// </summary>
    [WebService(Namespace = "http://www.couvax.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Products : System.Web.Services.WebService
    {
        [WebMethod]
        public List<spListProductsArrayResult> ListProductsArray(int MerchantId, string Password, string Array)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductsArray(MerchantId, Array).ToList();
        }
        [WebMethod]
        public List<spListProductsByPriceRangeResult> ListProductsByPriceRange(int MerchantId, string Password, decimal MinPrice, decimal MaxPrice)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductsByPriceRange(MerchantId, MinPrice,MaxPrice).ToList();
        }
        [WebMethod]
        public List<spListProductsOnHomePageResult> ListProductsOnHomePage(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductsOnHomePage(MerchantId).ToList();
        }
        [WebMethod]
        public List<bv_mostsoldResult> BvMostSold(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.bv_mostsold().ToList();
        }
        [WebMethod]
        public List<spListPromotedProductsResult> ListPromotedProducts(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListPromotedProducts(MerchantId).ToList();
        }
        [WebMethod]
        public List<spGetProductResult> GetProduct(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetProduct(MerchantId, ProductId).ToList();
        }
        [WebMethod]
        public List<spGetSkuResult> GetSku(int MerchantId, string Password, int Sku)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetSku(MerchantId, Sku).ToList();
        }
        [WebMethod]
        public List<spListProductPropertiesResult> ListProductProperties(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductProperties(MerchantId, ProductId).ToList();
        }
        [WebMethod]
        public List<spListProductSortsResult> ListProductSorts(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductSorts(MerchantId, ProductId).ToList();
        }
        [WebMethod]
        public List<spListAllProductsResult> ListAllProducts(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListAllProducts(MerchantId).ToList();
        }
        [WebMethod]
        public void SetFreeFieldsProducts(int MerchantId, string Password, int ProductId, string FreeField1, string FreeField2)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetFreeFieldsProducts(MerchantId,ProductId,FreeField1,FreeField2);
        }
        [WebMethod]
        public List<spListMostSoldProductsResult> ListMostSoldProducts(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListMostSoldProducts(MerchantId).ToList();
        }
        [WebMethod]
        public List<spListMostLikedProductsResult> ListMostLikedProducts(int MerchantId, string Password, string SocialNetwork)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListMostLikedProducts(MerchantId,SocialNetwork).ToList();
        }
        [WebMethod]
        public void SetProductSocialParameters(int MerchantId, string Password, int ProductId, string SocialNetwork, string Parameter, int Value)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetProductSocialParameters(MerchantId, ProductId,SocialNetwork,Parameter,Value);
        }
        [WebMethod]
        public List<spListProductsByCategoryResult> ListProductsByCategory(int MerchantId, string Password, string Order, int CategoryId, int PageNumber, int RecordsPerPage)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            if (Order.ToLower() != "name" &&
                Order.ToLower() != "price ascending" &&
                Order.ToLower() != "price descending" &&
                Order.ToLower() != "profit" &&
                Order.ToLower() != "priority")
            {
                throw new UnauthorizedAccessException("Invalid Order Parameter");
            }
            return SqlServer.spListProductsByCategory(MerchantId, Order.ToLower(),CategoryId,PageNumber,RecordsPerPage).ToList();
        }
        [WebMethod]
        public List<spListProductsBySortResult> ListProductsBySort(int MerchantId, string Password, string Order, int SortId, int SortCriteriaId,  int PageNumber, int RecordsPerPage)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            if (Order.ToLower() != "name" &&
                Order.ToLower() != "price ascending" &&
                Order.ToLower() != "price descending" &&
                Order.ToLower() != "profit" &&
                Order.ToLower() != "priority")
            {
                throw new UnauthorizedAccessException("Invalid Order Parameter");
            }
            return SqlServer.spListProductsBySort(MerchantId, Order.ToLower(), SortId,SortCriteriaId, PageNumber, RecordsPerPage).ToList();
        }
        [WebMethod]
        public int GetProductId(int MerchantId, string Password, string Tag)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetProductId(MerchantId, Tag).Single().Id;
        }
        [WebMethod]
        public void SetProductTag(int MerchantId, string Password, int ProductId, string Tag)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetProductTag(MerchantId, ProductId, Tag);
        }
        [WebMethod]
        public void GenerateProductTag(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            var this_product = SqlServer.spGetProduct(MerchantId, ProductId).Single();
            string a = this_product.Name;
            a = a.ToLower();
            a = a.Trim();
            a = a.Replace("  ", "");
            a = a.Replace("  ", "");
            a = a.Replace("  ", "");
            a = a.Replace("  ", "");
            a = a.Replace(" ", "-");
            a = a.Replace(".", "");
            a = a.Replace("%", "");
            a = a.Replace("+", "-");
            a = a.Replace(",", "");
            a = a.Replace(".", "");
            a = a.Replace("/", "-");
            a = a.Replace("\\", "-");
            a = a.Replace("*", "");
            a = a.Replace("--", "-");
            a = a.Replace("--", "-");
            a = a.Replace("--", "-");
            char b = (char)34;
            a = a.Replace(b.ToString(), "");
            string s = a.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < s.Length; k++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(s[k]);
                }
            }
            a = sb.ToString();
            SqlServer.spSetProductTag(MerchantId, ProductId, a);
        }
        [WebMethod]
        public List<spFilterSkuResult> FilterSku(int MerchantId, string Password,int ProductId,int PropertyId1, int PropertyItemId1,int PropertyId2, int PropertyItemId2 )
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spFilterSku(MerchantId,ProductId,PropertyId1,PropertyItemId1,PropertyId2,PropertyItemId2).ToList();
        }
        [WebMethod]
        public List<spListProductsWithNoAssignmentsResult> ListProductsWithNoAssignments(int MerchantId, string Password, string Type, int PropertyId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            if (Type.ToLower() != "category" && Type.ToLower() != "sort")
            {
                throw new UnauthorizedAccessException("Invalid Type.");
            }
            return SqlServer.spListProductsWithNoAssignments(MerchantId, Type.ToLower(),PropertyId).ToList();
        }
        [WebMethod]
        public List<spFilterProductsSortResult> FilterProductsSort(int MerchantId, string Password, int PropertyId, string ArrayProducts, string ArrayPropertyItems)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spFilterProductsSort(MerchantId, ArrayProducts, PropertyId, ArrayPropertyItems).ToList();
        }
        [WebMethod]
        public List<spInsertNewProductResult> InsertNewProduct(int MerchantId, string Password, string Name, string Tag, string RawDescription, string HtmlDescription, decimal SalePrice, decimal BuyPrice, bool FreeDelivery, int Weight, int Height,int Depth, int Width,string Keywords, int Priority,int DeadLine,bool IsFeatured, bool DisplayInHome, bool HavingSale, int ?PrimaryProperty,int ?SecundaryProperty)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spInsertNewProduct(
                MerchantId, Name, Tag,RawDescription,HtmlDescription,
                SalePrice, BuyPrice,FreeDelivery,
                Weight, Height, Depth, Width,
                Keywords, Priority,DeadLine,
                IsFeatured,DisplayInHome,HavingSale, PrimaryProperty,
                SecundaryProperty).ToList();
        }
        [WebMethod]
        public void SetProductSort(int MerchantId, string Password, int ProductId, int SortId,int ItemId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetProductSort(MerchantId,ProductId,SortId,ItemId);
        }
        [WebMethod]
        public void InsertProductCategory(int MerchantId, string Password, int ProductId, int CategoryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spInsertProductCategory(MerchantId, ProductId, CategoryId);
        }
        [WebMethod]
        public List<spInsertNewSkuResult> InsertNewSku(int MerchantId, string Password, int ProductId, string SkuOnWms, int TrueStock , int VirtualStock, int ?PrimaryPropertyItem, int ?SecundaryPropertyItem)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spInsertNewSku(MerchantId,ProductId,SkuOnWms,TrueStock,VirtualStock,
                PrimaryPropertyItem,SecundaryPropertyItem).ToList();
        }
        [WebMethod]
        public List<spListSkuByProductResult> ListSkuByProduct(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListSkuByProduct(MerchantId, ProductId).ToList();
        }
        [WebMethod]
        public List<spListActiveSkusResult> ListActiveSkus(int MerchantId, string Password)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListActiveSkus(MerchantId).ToList();
        }
        [WebMethod]
        public void SetDefaultSku(int MerchantId, string Password, int ProductId, int Sku)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetDefaultSku(MerchantId, ProductId,Sku);
        }
        [WebMethod]
        public void ActivateProduct(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spActivateProduct(MerchantId, ProductId);
        }
        [WebMethod]
        public void DeactivateProduct(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spDeactivateProduct(MerchantId, ProductId);
        }
        [WebMethod]
        public void SetSkuStock(int MerchantId, string Password, string StockType, int ?Sku, string SkuWms, int NewStockPosition)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetSkuStock(MerchantId, System.Convert.ToChar(StockType),Sku,SkuWms,NewStockPosition);
        }
        [WebMethod]
        public void SetProductComercialParameters(int MerchantId, string Password, int ProductId, decimal SalePrice, decimal BuyPrice, bool FreeFreight, int DeliveryDays)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetProductComercialParameters(MerchantId, ProductId, SalePrice, BuyPrice, FreeFreight,DeliveryDays);
        }
        [WebMethod]
        public void SetProductDimensions(int MerchantId, string Password, int ProductId, int Weight, int Height, int Width, int Depth)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetProductDimensions(MerchantId, ProductId,Weight,Height,Depth,Width);
        }
        [WebMethod]
        public void SetProductContent(int MerchantId, string Password, int ProductId, string Name, string RawDescription,string HtmlDescription,string KeyWords)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            SqlServer.spSetProductContent(MerchantId, ProductId, Name, RawDescription, HtmlDescription, KeyWords);
        }
        [WebMethod]
        public List<spGetSkuByWmsResult> GetSkuByWms(int MerchantId, string Password, string SkuWms)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetSkuByWms(MerchantId, SkuWms).ToList();
        }
        [WebMethod]
        public List<spGetCrossSellingListResult> GetCrossSellingList(int MerchantId, string Password, int ProductId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spGetCrossSellingList(MerchantId, ProductId).ToList();
        }
        [WebMethod]
        public List<spListProductsByStatusResult> ListProductsByStatus(int MerchantId, string Password, bool Status)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            if (SqlServer.spValidateRequest(MerchantId, Password).Single().Result == 0) { throw new UnauthorizedAccessException("Access is Denied"); }
            return SqlServer.spListProductsByStatus(MerchantId, Status).ToList();
        }
    }
}
